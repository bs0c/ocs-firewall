# OCS-Firewall

This is my attempt to write template rules for nftables. It is not possible at all, but as a result I have the helpful script to write and remove settings. And yes, I know you know about Ansible.


## Config

Your config needs to be in `./sets/<your_dir>`.

```
sets/
└── <your_dir>
    ├── nftables.conf
    └── nftables.d
        ├── ocs-firewall.conf
        └── ...
```

If you want to install other tools on the system, put them in <your_dir>/<path-to-destination>. It must be named by rule mount units in systemd, see `man systemd.mount.5`. If you are installing your units for systemd, set the prefix to `01-` so that they run in order.


## Usage

* The script will add a tag to the beginning of each file to easily remove all configs from `/etc` and  `/usr/bin`.
* Each configuration may have a `nftalbes.d/ocs-firewall.conf`. Usualy it will contain global variables for nftables. The script after copying files differs them in source and destination. If they are the same, the script will not run any services. To change this behavior use the `--skip-check` flag and see the help.

`./ocs-firewall -d server --debug`

Install the settings from `./sets/server`, add `counter` to each rule to see hooked packets and enable logging of dropped packets.

`./ocs-firewall -h`

for help


## License
MIT
